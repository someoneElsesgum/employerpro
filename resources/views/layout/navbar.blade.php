<nav class="navbar navbar-default">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">EmployerPro</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="{{ route('beranda') }}"><span class="fa fa-home"></span> Beranda <span class="sr-only">(current)</span></a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-book"></span> Master Data <span class="caret"></span> </a>
					<ul class="dropdown-menu">
						<li><a href="{{ route('yo') }}"><span class="fa fa-television"></span> Pegawai</a></li>
						<li><a href="#"><span class="fa fa-list"></span> Golongan</a></li>
						<li><a href="#"><span class="fa fa-star"></span> Jabatan</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-print"></span> Report <span class="caret"></span> </a>
					<ul class="dropdown-menu">
						<li><a href="{{ url('show') }}"><span class="fa fa-television"></span> Pegawai</a></li>
						<li><a href="{{ url('show') }}"><span class="fa fa-list"></span> Month</a></li>
					</ul>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-wrench"></span> Konfigurasi <span class="caret"></span> </a>
					<ul class="dropdown-menu">
						<li><a href="{{ url('show') }}"><span class="fa fa-television"></span> Data User</a></li>
						<li><a href="#"><span class="fa fa-plus"></span> Ubah Password</a></li>
						<li class="divider"></li>
						<li><a href="{{ url('/') }}"><span class="fa fa-sign-out"></span> Sign Out</a></li>
					</ul>
				</li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>