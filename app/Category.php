<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['nm_gol'];

    public function employ()
    {
    	return $this->hasMany(Employer::class, 'id_golongan');
    }
}
