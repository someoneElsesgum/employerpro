<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employer;
use App\Category;
use App\Position;
use Carbon\Carbon;

class EmpController extends Controller
{
    public function index()
    {
    	// $emp = DB::table('employers')
    	// 		->leftJoin('categories','categories.id','employers.id_golongan')
    	// 		->leftJoin('positions','positions.id','employers.id_jabatan')
    	// 		->get();
    	$emp = Employer::with('cat','pos')
            ->orderBy('updated_at','desc')
            ->paginate(10);

    	// dd($emp);
    	return view('emp',compact('emp'));
    }

    public function add()
    {
    	$cat = Category::all();
    	$pos = Position::all();
    	return view('crud.add_peg',compact('cat','pos'));
    }

    public function store(Request $request)
    {
    	$new = new Employer;
    	$new->nama = $request->nm;
    	$new->alamat = $request->alamat;
    	$new->tgl_lahir = $request->tgllhr;
    	$new->id_golongan = $request->gol;
    	$new->id_jabatan = $request->jab;
    	$new->save();

    	return redirect('admin/pegawai')->with(['success' => 'Berhasil disimpan']);
    }

    public function hapus(Employer $id)
    {
    	$id->delete();
    	return redirect('admin/pegawai');
    }

    public function edit($id)
    {
        $category = Category::all();
        $position = Position::all();
    	$editem = Employer::with('cat','pos')
                    ->find($id);
    	return view('crud.edit_peg',compact('editem','category','position'));
    }

    public function update(Request $request, $id)
    {
        $today = Carbon::now('Asia/Jakarta');
        $data = Employer::where('id',$id)->first();
        $data->nama = $request->nm;
        $data->alamat = $request->alamat;
        $data->tgl_lahir = $request->tgllhr;
        $data->id_golongan = $request->gol;
        $data->id_jabatan = $request->jab;

        $data->update();        
        return redirect('admin/pegawai')->with(['success' => 'Berhasil diedit']);

    }
}
