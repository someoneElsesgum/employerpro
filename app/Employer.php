<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employer extends Model
{
    protected $fillable = ['nama','alamat','tgl_lahir','id_golongan','id_jabatan'];

    public function cat()
    {
    	return $this->belongsTo(Category::class, 'id_golongan');
    }

    public function pos()
    {
    	return $this->belongsTo(Position::class, 'id_jabatan');
    }
}
